// src/config.rs: application configuration module
//
// Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

use dotenv::dotenv;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub db_path: String,
    pub api_url: String,
}

impl Config {
    pub fn load() -> Self {
        let mut cfg = config::Config::new();

        dotenv().ok();

        cfg.merge(config::Environment::with_prefix("iprec"))
            .unwrap();

        match cfg.try_into() {
            Ok(c) => c,
            Err(err) => {
                eprintln!("Configuration error: {}", err);

                std::process::exit(1);
            }
        }
    }
}
