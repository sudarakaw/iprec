// src/main.rs: main entry point for the iprec program.
//
// Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

#[tokio::main]
async fn main() {
    let cfg = iprec::Config::load();
    let ip_address = iprec::Api::get_ip_address(cfg.api_url).await;

    let mut db = iprec::DB::open(cfg.db_path).await;
    db.update(ip_address.split(".").collect()).await;
}
