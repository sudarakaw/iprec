// src/lib.rs: main module for the iprec program.
//
// Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

mod api;
mod config;
mod db;

pub use crate::api::Api;
pub use crate::config::Config;
pub use crate::db::DB;
