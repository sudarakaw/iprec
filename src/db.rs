// src/db.rs: sub-routines for database access
//
// Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

use sqlx::{Connect, SqliteConnection};

pub struct DB {
    conn: SqliteConnection,
}

impl DB {
    pub async fn open(db_path: String) -> DB {
        let mut conn = SqliteConnection::connect(format!("sqlite://{}", db_path))
            .await
            .unwrap();

        sqlx::query(
            "CREATE TABLE if not exists `ip`
        ( block1 TINYINT
        , block2 TINYINT
        , block3 TINYINT
        , block4 TINYINT
        , check_time DATETIME DEFAULT CURRENT_TIMESTAMP
        );
        ",
        )
        .execute(&mut conn)
        .await
        .unwrap();

        DB { conn }
    }

    pub async fn update(&mut self, ip_blocks: Vec<&str>) {
        sqlx::query("insert into `ip` (block1, block2, block3, block4) values(?, ?, ?, ?);")
            .bind(ip_blocks[0])
            .bind(ip_blocks[1])
            .bind(ip_blocks[2])
            .bind(ip_blocks[3])
            .execute(&mut self.conn)
            .await
            .unwrap();
    }
}
