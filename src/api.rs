// src/api.rs: sub-routines to call API to get IP Address
//
// Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

pub struct Api;

impl Api {
    pub async fn get_ip_address(api_url: String) -> String {
        let result = reqwest::get(&api_url)
            .await
            .and_then(|resp| Ok(resp.text()));

        match result {
            Ok(ip) => ip.await.unwrap(),
            Err(err) => {
                eprintln!("Failed to obtain IP Address:\n{}", err);

                std::process::exit(1);
            }
        }
    }
}
