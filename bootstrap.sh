# bootstrap.sh: autotools bootsreap script
#
# Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
#
# This program comes with ABSOLUTELY NO WARRANTY;
# This is free software, and you are welcome to redistribute it and/or modify it
# under the terms of the BSD 2-clause License. See the LICENSE file for more
# details.
#

autoreconf --install
automake --add-missing --copy >/dev/null 2>&1
